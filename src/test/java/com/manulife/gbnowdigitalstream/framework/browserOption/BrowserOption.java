package com.manulife.gbnowdigitalstream.framework.browserOption;

public abstract class BrowserOption
{
    public abstract void setHeadless(boolean value);
    
}
