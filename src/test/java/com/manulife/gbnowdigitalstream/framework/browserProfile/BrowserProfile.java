package com.manulife.gbnowdigitalstream.framework.browserProfile;

public abstract class BrowserProfile
{
    public abstract Object createProfile();
}
