package com.manulife.gbnowdigitalstream.runner;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.manulife.gbnowdigitalstream.framework.common.CommonVariable;
import com.vimalselvam.cucumber.listener.ExtentProperties;
import com.vimalselvam.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(glue = "com/manulife/gbnowdigitalstream/stepdefinition", features = "src/test/java/com/manulife/gbnowdigitalstream/feature", plugin = { "html:target/cucumber-htmlreport",
	"json:target/cucumber-report.json", "pretty:target/cucumber-pretty.txt", "usage:target/cucumber-usage.json",
	"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:" }, tags = { "@Regression" }, monochrome = true)
public class TestRunner extends AbstractTestNGCucumberTests {

    // AND tags = { "@Regression","@p1" })
    // OR tags = { "@Regression,@p1" })

    @BeforeSuite(alwaysRun = true)
    public static void setup() throws IOException {
	ExtentProperties extentProperties = ExtentProperties.INSTANCE;
	extentProperties.setReportPath(CommonVariable.currentRootDir + "/output/myreport.html");
    }

    @AfterSuite(alwaysRun = true)
    public void getResult() throws IOException {
	Reporter.loadXMLConfig(new File(CommonVariable.currentRootDir + "/src/test/resources/extent-config.xml"));
	Reporter.setSystemInfo("user", System.getProperty("user.name"));
	Reporter.setSystemInfo("os", "Mac OSX");
	Reporter.setTestRunnerOutput("Automation execution of GB Now Project");
	Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
	Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
	Reporter.setSystemInfo("Machine", "Windows 10" + "64 Bit");
	Reporter.setSystemInfo("Selenium", "3.7.0");
	Reporter.setSystemInfo("Maven", "3.5.2");
	Reporter.setSystemInfo("Java Version", "1.8.0_151");
    }
}
